package sk.seidl.example.spring5webapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import sk.seidl.example.spring5webapp.repositories.AuthorRepository;

/**
 * @author Matus Seidl (5+3)
 * 2017-11-28
 */
@Controller
public class AuthorController {

    private AuthorRepository authorRepository;

    @Autowired
    public AuthorController(AuthorRepository bookRepository) {
        this.authorRepository = bookRepository;
    }

    @RequestMapping("/authors")
    public String getBooks(Model model) {
        model.addAttribute("authors", authorRepository.findAll());
        return "authors";
    }
}
