package sk.seidl.example.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import sk.seidl.example.spring5webapp.models.Book;

/**
 * @author Matus Seidl (5+3)
 * 2017-11-28
 */
public interface BookRepository extends CrudRepository<Book,Long> {
}
