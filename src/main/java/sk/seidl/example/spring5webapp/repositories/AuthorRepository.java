package sk.seidl.example.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import sk.seidl.example.spring5webapp.models.Author;

/**
 * @author Matus Seidl (5+3)
 * 2017-11-28
 */
public interface AuthorRepository extends CrudRepository<Author, Long> {
}
