package sk.seidl.example.spring5webapp.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Matus Seidl (5+3)
 * 2017-11-28
 */

@Getter
@Setter
@EqualsAndHashCode(exclude = {"name", "address"})
@ToString
@Entity
public class Publisher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;
}

